@component('mail::message')
# Bienvenido

{{$email}},

Gracias por registrarse en nuestra plataforma.

Cordialmente,<br>
{{ config('app.name') }}
@endcomponent