@component('mail::message')
# Reporte

A continuación se envía un informe de los usuarios registrados: <br>

<table class="customTable" border=1>
    <thead>
        <tr>
            <th>País</th>
            <th>Total</th>
        </tr>
    </thead>
    @foreach ($data as $item)
    <tbody>
        <tr>
            <td>{{$item->pais}}</td>
            <td style="text-align:right;">{{$item->total}}</td>
        </tr>
    </tbody>
    @endforeach
</table>

<br>

Cordialmente,<br>
{{ config('app.name') }}
@endcomponent