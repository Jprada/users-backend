<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization, enctype');

Route::get('usuario', 'App\Http\Controllers\UsuariosController@show')->name('usuario.show');
Route::post('usuario', 'App\Http\Controllers\UsuariosController@create')->name('usuario.create');
Route::put('usuario', 'App\Http\Controllers\UsuariosController@edit')->name('usuario.edit');
Route::delete('usuario', 'App\Http\Controllers\UsuariosController@destroy')->name('usuario.destroy');

