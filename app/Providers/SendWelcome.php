<?php

namespace App\Providers;

use App\Providers\Created;
use App\Mail\NotifyMail;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class SendWelcome
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Providers\Created  $event
     * @return void
     */
    public function handle(Created $event)
    {
        $mail = $event->email;
        Mail::to($mail)->send(new NotifyMail($mail));
        return new JsonResponse(
            [
                'success' => true, 
                'message' => "Gracias por registrarte, revisa el email."
            ], 
            200
        );
    }
}
