<?php

namespace App\Providers;

use App\Providers\Created;
use App\Models\Usuarios;
use App\Mail\ReportMail;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use DB;

class SendReport
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Providers\Created  $event
     * @return void
     */
    public function handle(Created $event)
    {
        $datos = Usuarios::select([DB::raw('count(pais) as total'),'pais'])->groupBy('pais')->get();

        $mail = env('ADMIN_EMAIL');
        Mail::to($mail)->send(new ReportMail($datos));
        return new JsonResponse(
            [
                'success' => true, 
                'message' => "El reporte ha sido enviado!"
            ], 
            200
        );
    }
}
