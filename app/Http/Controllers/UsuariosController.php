<?php

namespace App\Http\Controllers;

use App\Models\Usuarios;
use Illuminate\Http\Request;
use App\Providers\Created;
use DB;

class UsuariosController extends Controller
{
    public function show()
    {
        try {
            $usuarios = Usuarios::get();
            return $usuarios;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    public function create(Request $request)
    {
        try {
            $existe_cedula = Usuarios::select('cedula')->where('cedula',$request->cedula)->get();
            $existe_email = Usuarios::select('email')->where('email',$request->email)->get();
            if (count($existe_cedula) > 0) {
                $respuesta = response()->json(['tipo' => 'error','respuesta' => 'La cédula ya existe en la base de datos.'],200);
            } else if (count($existe_email) > 0) {
                $respuesta = response()->json(['tipo' => 'error','respuesta' => 'El e-mail ya existe en la base de datos.'],200);
            } else if (count($existe_cedula) === 0 && count($existe_email) === 0) {
                $usuario = new Usuarios();
                $usuario->nombres = $request->nombres;
                $usuario->apellidos = $request->apellidos;
                $usuario->cedula = $request->cedula;
                $usuario->pais = $request->pais;
                $usuario->email = $request->email;
                $usuario->direccion = $request->direccion;
                $usuario->celular = $request->celular;
                $usuario->save();
                event(new Created($request->email));
                $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se creó el registro con éxito'],200);
            }
        } catch (Exception $e) {
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);
        }
        return $respuesta;
    }
    public function edit(Request $request)
    {
        $id = $request->id;
        try {
            $usuario = Usuarios::find($id);
            $usuario->nombres = $request->nombres;
            $usuario->apellidos = $request->apellidos;
            $usuario->cedula = $request->cedula;
            $usuario->pais = $request->pais;
            $usuario->email = $request->email;
            $usuario->direccion = $request->direccion;
            $usuario->celular = $request->celular;
            $usuario->save();
            $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se actualizó el registro con éxito'],200);
        } catch (Exception $e) {
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);
        }
        return $respuesta;
    }
    public function destroy(Request $request)
    {
        $id = $request->id;
        try {
            Usuarios::where('id',$id)->delete();
            $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se eliminó el registro con éxito'],200);
        } catch (Exception $e) {
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);
        }
        return $respuesta;
    }
}
